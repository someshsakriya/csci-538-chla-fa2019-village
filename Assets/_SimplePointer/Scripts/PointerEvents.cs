﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PointerEvents : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{

    [SerializeField] private Color normalColor = Color.green;
    [SerializeField] private Color enterColor = Color.blue;
    [SerializeField] private Color downColor = Color.yellow;
    [SerializeField] private UnityEvent OnClick = new UnityEvent();
    GameObject SubmitGameObject;
    Button ButtonGameObject;
    private MeshRenderer meshRenderer = null;
    int count=0;

    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        meshRenderer.material.color = enterColor;
        print("Enter");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        meshRenderer.material.color = normalColor;
        print("Exit");
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        meshRenderer.material.color = downColor;
        print("Down");
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        meshRenderer.material.color = enterColor;
        print("Up");
    }

    GameObject findGameObject(string name)
    {
        return GameObject.Find(name);
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        print("on pointer click");
        OnClick.Invoke();
        print("clicked invoke");
        // print( gameObject.GetComponent<Button>());
        // Button btn = gameObject.GetComponent<Button>();
        // btn.onClick.AddListener(clickSubmit);
        clickSubmit();

        
      
    }

    void clickSubmit()
    {
        //OnClick.Invoke();
        print("clicked submit");
        count += 1;
        print("count is:"+count);
        // meshRenderer.material.color = enterColor;
        print("Arushi Click");
        SubmitGameObject = findGameObject("LandingPage");
        print("Submit");
        SubmitGameObject.SetActive(false);
        //  meshRenderer.material.color = enterColor;
    }

    public void ToGame1()
    {
        print("Submit asdasd");
    }

    
}
