﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodsToCampFire : MonoBehaviour
{
    GameObject go, top, left, right, mid, flame, fireMain;
    /*public float seconds = 4f;
    public Vector3 velocity = Vector3.zero;
    public float timer;*/
    //public Vector3 Point;
   // public Transform target;
    public Vector3 DifferenceIn, DifferenceEx, Fire, start, Point1, Point2, DifferencePause;
    //public Vector3 start;
    //public float DifferenceEx;
    //public float speed;
     Light lt;
    int count = 0;
    void Start()
    {
        //go = GetComponent<Renderer>().gameObject;
        top = GameObject.Find("TopLog");
        left = GameObject.Find("LeftLog");
        right = GameObject.Find("RightLog");
        mid = GameObject.Find("MiddleLog");
        fireMain = GameObject.Find("IntenseFire");
        flame = GameObject.Find("FlameGame");
        lt = fireMain.GetComponent<Light>();
        //Point = new Vector3(2.68f, -4.27f, -1.85f);
        Point1 = new Vector3(1.1f, -4.27f, -1.85f);
        Point2 = new Vector3(2.68f, -4.27f, -1.85f);
        Fire = new Vector3(2.68f, -4.27f, 0f);

        /*start = go.transform.localPosition;
        DifferenceIn = Point - start;
        DifferenceEx = Fire - Point;*/

    }

    // Update is called once per frame
    void Update()
    {
        if (count < 12)
        {
            // INHALE
            if (Input.GetKeyDown("a"))
            {
                if (count == 0 || count == 3 || count == 6 || count == 9)
                {
                    if (count == 0)
                    {
                        go = top;
                        start = go.transform.localPosition;
                        DifferenceIn = Point1 - start;
                    }
                    if (count == 3)
                    {
                        go = right;
                        start = go.transform.localPosition;
                        DifferenceIn = Point1 - start;
                    }
                    if (count == 6)
                    {
                        go = mid;
                        start = go.transform.localPosition;
                        DifferenceIn = Point1 - start;
                    }
                    if (count == 9)
                    {
                        go = left;
                        start = go.transform.localPosition;
                        DifferenceIn = Point1 - start;
                    }
                    // Debug.Log(go);
                    // Debug.Log(go.transform.position);
                    // Debug.Log(go.transform.localPosition);
                    //go.transform.localPosition = Vector3.MoveTowards(go.transform.localPosition, Point1, 0.09f);
                    // On inhalation, while inhaling and this condition
                    //DifferenceIn.x = DifferenceIn.x / (float)300.0;
                    //DifferenceIn.y = DifferenceIn.y / (float)300.0;
                    //DifferenceIn.z = DifferenceIn.z / (float)300.0;

                    while (go.transform.localPosition != Point1)
                    {
                        
                        go.transform.localPosition += DifferenceIn / 300.0f;
                    }
                    count++;
                }
            }



            //PAUSE
            if (Input.GetKeyDown("d"))
            {
                if (count == 1 || count == 4 || count == 7 || count == 10)
                {
                    if (count == 1)
                    {
                        go = top;
                        start = go.transform.localPosition;
                        DifferencePause = Point2 - start;
                    }
                    if (count == 4)
                    {
                        go = right;
                        start = go.transform.localPosition;
                        DifferencePause = Point2 - start;
                    }
                    if (count == 7)
                    {
                        go = mid;
                        start = go.transform.localPosition;
                        DifferencePause = Point2 - start;
                    }
                    if (count == 10)
                    {
                        go = left;
                        start = go.transform.localPosition;
                        DifferencePause = Point2 - start;
                    }
                    while (go.transform.localPosition != Point2)
                    {
                        go.transform.localPosition += DifferencePause / 300.0f;
                    }
                    count++;
                }
            }




            // EXHALE
            if (Input.GetKeyDown("s"))
            {
                if (count == 2 || count == 5 || count == 8 || count == 11)
                {
                    if (count == 2)
                    {
                        go = top;
                        start = go.transform.localPosition;
                        DifferenceEx = Fire - start;
                    }
                    if (count == 5)
                    {
                        go = right;
                        start = go.transform.localPosition;
                        DifferenceEx = Fire - start;
                    }
                    if (count == 8)
                    {
                        go = mid;
                        start = go.transform.localPosition;
                        DifferenceEx = Fire - start;
                    }
                    if (count == 11)
                    {
                        go = left;
                        start = go.transform.localPosition;
                        DifferenceEx = Fire - start;
                    }
                    // On exhalation, while exhaling and this condition
                    while (go.transform.localPosition != Fire)
                    {
                        go.transform.localPosition += DifferenceEx / 300.0f;
                    }
                    //for (int i = 0; i < 10; i++)
                    //{
                    flame.transform.localScale += new Vector3(0.19f, 0.19f, 0.19f);
                    lt.intensity += 0.05f;
                    //}
                    count++;
                }
            }
        }


            //float step = speed * Time.deltaTime;
            //Vector3 targetPos = target.TransformPoint(new Vector3(2.68f, -4.27f, -1.85f));
            //go.transform.localPosition = Vector3.MoveTowards(go.transform.localPosition, targetPos, step);
            /*timer = 0f;
            while (timer <= seconds)
            {
                // basic timer
                timer += Time.deltaTime;
                //timer += 0.01f;
                // percent is a 0-1 float showing the percentage of time that has passed on our timer!
                percent = timer / seconds;
                // multiply the percentage to the difference of our two positions
                // and add to the start
                go.transform.localPosition = start + Difference * percent;
            }*/

            //go.transform.localPosition = Vector3.SmoothDamp(go.transform.localPosition, targetPos, ref velocity, seconds);    

        /*if (count < 1)
        {
            count++;
            Debug.Log(count);*/
        /*for (int i = 0; i < 40; i++)
        {
                go.transform.localPosition += Difference / 40;
                new WaitForSeconds(0.1f);
            //go.transform.localPosition += new Vector3(0.059f, -0.107f, -0.041f);
            //transform.Translate += new Vector3(0.3f, -0.3f, 0.3f);
            // lt.intensity += 0.008f;
        }*/
            //mat.SetColor("_EmissionColor", Color.white);
        //}
       // }
    }
}
