﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class update_gui_script : MonoBehaviour
{
    // public void update_gui(string text)
    // {
    //     // text_sample.Text = (text_sample.Text + 1).ToString();
    //     Text txt = transform.Find("Text").GetComponent<Text>();
    //     int int_text = Int32.Parse(txt.text);
    //     txt.text = (int_text + 1).ToString();
    // }

    public void add_ball(Text levelObj)
    {
        int int_text = int.Parse(levelObj.text);
        // print(int_text);
        if(int_text < 5)
            levelObj.text = (int_text + 1).ToString();
    }

    public void subtract_ball(Text levelObj)
    {
        int int_text = int.Parse(levelObj.text);
        if(int_text > 1)
            levelObj.text = (int_text - 1).ToString();
    }

    public void onExit() {
        Application.Quit();
    }

}
