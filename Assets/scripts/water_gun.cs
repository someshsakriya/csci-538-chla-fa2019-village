﻿using System.Collections;
using UnityEngine;
using OVRTouchSample;


public class water_gun : MonoBehaviour
{
    float Target;
    GameObject waterGun;
    GameObject inhale;
    GameObject campFlame;
    public GameObject campFire;
    public GameObject lake;
    GameObject lakeClone;
    GameObject campFireClone;
    public GameObject exhale;
    GameObject dragonFireClone;
    GameObject flameThrowerFire;
    ParticleSystem flameThrowerParticleSystem;
    GameObject flameThrowerSmoke;
    ParticleSystem flameThrowerSmokeParticleSystem;
    GameObject dragonClone;
    public GameObject dragon;
    public GameObject dragonFire;
    // for hut 2

    GameObject campFlame2;
    public GameObject campFire2;
    GameObject campFireClone2;
    bool dragon_move_right = false;
    bool hut1 = true;
    bool hut2 = false;
    int count = 0;

    // Start is called before the first frame update
    void Start()
    {
        UnityEngine.XR.InputTracking.disablePositionalTracking = true;
        //lake = GetComponent<Renderer>().gameObject;
        inhale = GameObject.Find("Inhale");
        inhale.SetActive(false);
        waterGun = GameObject.Find("water_gun");
        waterGun.SetActive(false);
        campFlame = GameObject.Find("CampFlame1");
        //campFlameParticleSystem = campFlame.GetComponent<ParticleSystem>();
        //campFlameParticleSystem.Stop();
        campFlame.SetActive(false);
        flameThrowerFire = GameObject.Find("FlamethrowerFire");
        flameThrowerParticleSystem = flameThrowerFire.GetComponent<ParticleSystem>();
        flameThrowerSmoke = GameObject.Find("FlamethrowerSmoke");
        flameThrowerSmokeParticleSystem = flameThrowerSmoke.GetComponent<ParticleSystem>();


        

        print("particle gameobject " + flameThrowerFire);
        print("particle " + flameThrowerParticleSystem.isPlaying);
        StartCoroutine(setCampOnFire(campFire, 1));
        /*lakeClone = Instantiate(lake);
        campFireClone = Instantiate(campFire);
        campFireClone.SetActive(false);
        lakeClone.SetActive(false);
        dragonClone = Instantiate(dragon);
        dragonClone.SetActive(false);*/
        // for hut 2
        campFlame2 = GameObject.Find("CampFlame2");
        //campFlameParticleSystem2 = campFlame2.GetComponent<ParticleSystem>();
        //campFlameParticleSystem2.Stop();
        campFlame2.SetActive(false);
        //campFire = GameObject.Find("FireEmbers1");
        /*campFireClone2 = Instantiate(campFire2);
        campFireClone2.SetActive(false);*/
    }

    // Update is called once per frame
    void Update()
    {
        OVRInput.Update();

        if ((OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) || Input.GetKeyDown("a") || Input.GetKeyDown(KeyCode.Space)) && count == 0)
        {
            print("INHALE................");
            StartCoroutine(run_inhale());
        }
        else if ((OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) || Input.GetKeyDown("z") || Input.GetKeyDown(KeyCode.Space)) && count == 1)


        {
            print("EXHALE");
            StartCoroutine(run_water_gun());
            //StartCoroutine(decrease_fire());
        }


    }

    IEnumerator run_inhale()
    {
        print("inhaling");
        inhale.SetActive(true);


        yield return new WaitForSeconds(1);
        for (int i = 0; i < 3; i++)
        {
            float speed = 40.0f;
            lake.transform.position -= lake.transform.up * speed * Time.deltaTime;
            yield return new WaitForSeconds(0.5f);
        } //set lake to false after this

        inhale.SetActive(false);
        count = 1;
        //StartCoroutine(run_water_gun());




    }

    IEnumerator run_water_gun()
    {
        print("exhaling");
        waterGun.SetActive(true);
        //int whilecnt = 0;
        print("run water oct");
        yield return new WaitForSeconds(1);
        print("here");
        //StartCoroutine(removeCampFire());
        if (hut1 == true)
        {
            print("hut 1 is set to true");
            for (int i = 0; i < 3; i++)
            {
                campFire.transform.localScale -= new Vector3(1.0f, 1.0f, 1.0f);
                yield return new WaitForSeconds(0.5f);
                //whilecnt += 1;
            }

            waterGun.SetActive(false);

            // dragon moves right
            dragon_move_right = true;
            //hut1 = false;
            //hut2 = true;
            count = 3;
            flameThrowerParticleSystem.Stop();
            flameThrowerSmokeParticleSystem.Stop();
            StartCoroutine(make_dragon_move_right());
            //make_dragon_move_right();
            print("skipped to next line moving to hut 2");
        } else if (hut2 == true)
        {
            print("hut 2 is set to true");
            for (int i = 0; i < 3; i++)
            {
                campFire2.transform.localScale -= new Vector3(1.0f, 1.0f, 1.0f);
                yield return new WaitForSeconds(1);
                //whilecnt += 1;
            }
            waterGun.SetActive(false);
            
            //hut1 = true;
            //hut2 = false;
            count = 3;
            // dragon moves right
            dragon_move_right = false;
            flameThrowerParticleSystem.Stop();
            flameThrowerSmokeParticleSystem.Stop();
            StartCoroutine(make_dragon_move_right());
            //make_dragon_move_right();
            print("skipped to next line moving towards hut1");
          

            waterGun.SetActive(false);
        } 
        
        

    }

    void reinitialize_dragon_for_hut2()
    {
        print("reinitializing for hut2");
        count = 0;
        hut1 = false;
        hut2 = true;
        //print("camp fire clone scale:" + campFireClone.transform.localScale);
        //campFireClone.SetActive(true);
        //campFire = Instantiate(campFireClone);
        //print("camp fire scale:" + campFire.transform.localScale);
        //campFireClone.SetActive(false);
        float speed = 10.0f;
        lake.transform.localPosition = new Vector3(13.64814f, 2.442589f, -0.2499993f);
        campFire.transform.localScale = new Vector3(3.0f, 3.0f, 3.0f);   
        flameThrowerParticleSystem.Play();
        flameThrowerSmokeParticleSystem.Play();
        //dragonFire.SetActive(true);
        campFire.SetActive(false);
        StartCoroutine(setCampOnFire(campFire2, 2));
    }

    void reinitialize_dragon_for_hut1()
    {
        print("reinit for hut 1");
        hut1 = true;
        hut2 = false;
        count = 0;
        /*print("camp fire clone scale:" + campFireClone2.transform.localScale);
        campFireClone2.SetActive(true);
        campFire2 = Instantiate(campFireClone2);
        print("camp fire scale:" + campFire.transform.localScale);
        campFireClone2.SetActive(false);*/
        float speed = 10.0f;
        lake.transform.localPosition =  new Vector3(13.64814f, 2.442589f, -0.2499993f);
        //lake.transform.position -= lake.transform.up * speed * Time.deltaTime;
        campFire2.transform.localScale = new Vector3(3.0f, 3.0f, 3.0f);
        campFire2.SetActive(false);
        print("fireactive 2 " + dragonFire.name);
        print("particle 2" + flameThrowerParticleSystem.isPlaying);
        flameThrowerParticleSystem.Play();
        flameThrowerSmokeParticleSystem.Play();
        StartCoroutine(setCampOnFire(campFire,1));
    }
    



    IEnumerator make_dragon_move_right()
    {
        print("in the coroutine: "+ dragon_move_right);
        count = 3;
        float speed = 60.0f;
        if (dragon_move_right == true)
        {
            while (dragon_move_right && dragon.transform.position.x <= -12.0f)
            {
                print("inside going to hut 2");
                count = 3;
                dragon.transform.position += dragon.transform.forward * speed * Time.deltaTime;
                Target = -12.0f;
                //dragon.transform.position = Vector3.MoveTowards(dragon.transform.position, new Vector3(Target, dragon.transform.position.y, dragon.transform.position.z), 1.0f);
                if (dragon_move_right && dragon.transform.position.x >= -12.0f)
                {
                    print("stopping at hut 2");
                    dragon_move_right = false;
                    dragon.transform.Rotate(0, 180, 0);
                    break;
                }
                yield return new WaitForSeconds(0.2f);
            }
            print("posn of dragon:" + dragon.transform.position.x);
            reinitialize_dragon_for_hut2();
        }
        else if (dragon_move_right == false)
        {
            //print(".....");

            while (!dragon_move_right && dragon.transform.position.x >= -21.0f)
            {
                print("inside going to hut 1");
                count = 3;
                dragon.transform.position += dragon.transform.forward * speed * Time.deltaTime;
                //yield return new WaitForSeconds(0.2f);
                if (!dragon_move_right && dragon.transform.position.x <= -21.0f)
                {
                    print("stopping at hut 1");
                    dragon_move_right = true;
                    dragon.transform.Rotate(0, 180, 0);
                    break;
                }
                yield return new WaitForSeconds(0.2f);
            }
            reinitialize_dragon_for_hut1();
        }

    }





    IEnumerator setCampOnFire(GameObject flame, int hut)
    {

        yield return new WaitForSeconds(2.50f);
        //dragonFire.SetActive(false);
        flameThrowerParticleSystem.Stop();
        flameThrowerSmokeParticleSystem.Stop();
        flame.SetActive(true);

        if (hut == 1)
        {
            campFire2.SetActive(false);
           // campFlameParticleSystem.Stop();
            //campFlameParticleSystem2.Play();
            exhale.transform.rotation = Quaternion.Euler(new Vector3(-5.67f, -376.39f, 23.8f));

            // exhale.transform.Rotate(0, 45, 0);
        } else if( hut == 2)
        {
            campFire.SetActive(false);
            //campFlameParticleSystem2.Stop();
            //campFlameParticleSystem.Play();
            exhale.transform.rotation = Quaternion.Euler(new Vector3(-5.67f, -405.2f, 23.8f));
            //exhale.transform.Rotate(0, -45, 0);
        }
    }

    IEnumerator removeWater()
    {
        yield return new WaitForSeconds(0.3f);
        waterGun.SetActive(false);
    }

    void removeCampFireFunc()
    {
        while (campFlame.transform.localScale.y > 0)
        {
            campFlame.transform.localScale -= new Vector3(0.0001f, 0.0001f, 0.0001f);
            removeCampFireFunc();
            //whilecnt += 1;
        }
    }
    IEnumerator removeCampFire()
    {
        yield return new WaitForSeconds(1);
        while (campFlame.transform.localScale.y > 0)
        {
            campFlame.transform.localScale -= new Vector3(0.01f, 0.01f, 0.01f);
            StartCoroutine(removeCampFire());
            //removeCampFire();
            //whilecnt += 1;
        }
        StartCoroutine(removeWater());
        //campFlame.SetActive(false);
        //count = 0;
    }

    IEnumerator decrease_fire()
    {
        int whilecnt = 0;
        while (whilecnt < 10)
        {
            print("scale is: " + campFlame.transform.localScale);
            campFlame.transform.localScale -= new Vector3(0.0f, 0.01f, 0.0f);
        }
        yield return new WaitForSeconds(1);
    }

}