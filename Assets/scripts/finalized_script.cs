using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class finalized_script : MonoBehaviour
{
    bool use_spirometer = true;
    System.Diagnostics.Stopwatch watch;

    enum BREATHING_STATE {EXHALE, INHALE, PAUSE};
    enum PHASES {STARTUP, DRAGON, DRAON_STARTED, WATER_LAKE_FILL, WATER_LAKE_FILL_STARTED, INHALE, EXHALE, STARTING_FIRE_AGAIN, CYCLE_DONE};
    BREATHING_STATE currentBreathingState = BREATHING_STATE.PAUSE;
    PHASES currentPhase = PHASES.STARTUP;

    //reward
    public GameObject star1;
    ParticleSystem star1ParticleSystem;
    
    public GameObject fail;
    ParticleSystem failParticleSystem;
    public GameObject celebration;
    ParticleSystem celebrationParticleSystem;

    // OSC
    GameObject oscGameObject;
    OSC oscScript;
    int inhaling_expected_force = 2200;
    int exhaling_expected_force = 1900;

    // DD Value shoudl be from 1-5
    int inhalation_time = 1;
    float[] inhalation_speed = {0.038f, 0.03f, 0.03f, 0.021f, 0.017f};
    float[] inhalation_wait_time = {0.03f, 0.05f, 0.07f, 0.07f, 0.07f};
    
    // DD Value shoudl be from 1-5
    int exhalation_time = 3;
    float[] exhalation_speed = {0.1f, 0.16f, 0.11f, 0.085f, 0.07f};
    float[] exhalation_wait_time = {0.02f, 0.05f, 0.06f, 0.06f, 0.06f};
    bool inhale_exhale_started = false;

    // Inhalation Exhalation
    GameObject waterGunGameObject;
    ParticleSystem waterGunParticleSystem;
    GameObject inhaleGameObject;
    ParticleSystem inhaleParticleSystem;
    List<Quaternion> waterGunRotationValues;

    // CampFire
    List<GameObject> campFlameGameObjectList;
    //List<ParticleSystem> campFlameParticleSystemList;
    List<GameObject> campEmberGameObjectList;
    //List<ParticleSystem> campEmberParticleSystemList;
    Vector3 fireFlameOriginalScale;
    Vector3 fireEmberOriginalScale;
    Vector3 zeroScale;
    float fireIncreaseRate = 0.01f;
    bool fire_out = false;

    // Water Lake
    GameObject waterLakeGameObject;
    Vector3 waterLakeOriginalLoctionVector;
    Vector3 waterLakeFinalLoctionVector;
    bool lake_filled = true;
    float water_lake_fill_speed = 0.01f;

    // Dragon
    GameObject dragonCommodityController;
    GameObject dragonGameObject;
    Animation dragonStationaryAnimation;
    GameObject flameThrowerGameObject;
    AudioSource flameThrowerAudioSource;
    GameObject flameThrowerFlameGameObject;
    ParticleSystem flameThrowerFlameParticleSystem;
    GameObject flameThrowerSmokeGameObject;
    ParticleSystem flameThrowerSmokeParticleSystem;
    List<Vector3> dragonFiringPositionList;
    List<Quaternion> dragonRotationValuesList;
    float dragonMovementSpeed = 0.10f;
    float dragonRotationSpeed = 1.0f;

    // Movement
    int totalTargets = 2;
    int currentTarget = 0;
    // DD variable for stats
    int completed_cycles = 0;
    int successful_cycle = 0;
    int cycle_failed = 0;
    int inhalation_failed = 0;
    int inhalation_success = 0;
    int inhalation_total = 0;
    int exhalation_failed = 0;
    int exhalation_success = 0;
    int exhalation_total = 0;

    //Score and Landing and Ending Page
    public GameObject stats;
    GameObject SubmitGameObject;
    GameObject GameOverGameObject;
    public GameObject scoreText;
    public GameObject totaltext;
    public GameObject exhalationtext;
    public GameObject inhalationtext;
    public GameObject successfultext;
    public GameObject maxCycle;
    GameObject ovrCamerRig;
    GameObject uiHelper;

    public AudioSource inhaleAudio;

    public AudioSource exhaleAudio;

    public AudioSource cheerAudio;

    public AudioSource failAudio;

    public AudioSource successAudio;

    // DD For maximum number of cycle
    int max_cycle = 5;
    bool gameFinished = false;

    bool inhale_completely_done = false;
    bool exhale_completely_done = false;

    GameObject findGameObject(string name)
    {
        return GameObject.Find(name);
    }

    ParticleSystem findParticleSystem(GameObject gameObject)
    {
        return gameObject.GetComponent<ParticleSystem>();
    }

    void initializeObejects()
    {
        // OSC
        if (use_spirometer)
        {
            oscGameObject = findGameObject("OSC");
            oscScript = oscGameObject.GetComponent<OSC>();
        }
        
        
        // Reward
        star1ParticleSystem = findParticleSystem(star1);
        failParticleSystem = findParticleSystem(fail);
        celebrationParticleSystem = findParticleSystem(celebration);
        star1ParticleSystem.Stop();
        failParticleSystem.Stop();
        celebrationParticleSystem.Stop();

        GameOverGameObject = findGameObject("EndPage");
        if(GameOverGameObject)
        {
            GameOverGameObject.SetActive(false);
        }
        stats.SetActive(false);

        // Inhalation Exhalation
        waterGunGameObject = findGameObject("water_gun");
        waterGunParticleSystem = findParticleSystem(waterGunGameObject);
        waterGunParticleSystem.Stop();
        inhaleGameObject = findGameObject("Inhale");
        inhaleParticleSystem = findParticleSystem(inhaleGameObject);
        inhaleParticleSystem.Stop();

        waterGunRotationValues = new List<Quaternion>();
        waterGunRotationValues.Add(Quaternion.Euler(new Vector3(-1.2f, -372.5f, 23.8f)));
        waterGunRotationValues.Add(Quaternion.Euler(new Vector3(-1.2f, -408.8f, 23.8f)));
        waterGunRotationValues.Add(Quaternion.Euler(new Vector3(-1.2f, -389.6f, 23.8f)));
        waterGunGameObject.transform.rotation = waterGunRotationValues[currentTarget];

        // CampFire
        campFlameGameObjectList = new List<GameObject>();
        //campFlameParticleSystemList = new List<ParticleSystem>();
        campEmberGameObjectList = new List<GameObject>();
        //campEmberParticleSystemList = new List<ParticleSystem>();

        fireFlameOriginalScale = new Vector3(3f, 3f, 3f);
        fireEmberOriginalScale = new Vector3(1f, 1f, 1f);
        zeroScale = new Vector3(0f, 0f, 0f);

        for (int i = 0; i < totalTargets; i++)
        {
            campFlameGameObjectList.Add(findGameObject("CampFlame" + (i + 1)));
            campFlameGameObjectList[i].transform.localScale = zeroScale;
            //campFlameParticleSystemList.Add(findParticleSystem(campFlameGameObjectList[i]));
            //campFlameParticleSystemList[i].Stop();
            
            campEmberGameObjectList.Add(findGameObject("CampEmbers" + (i + 1)));
            campEmberGameObjectList[i].transform.localScale = zeroScale;
            //campEmberParticleSystemList.Add(findParticleSystem(campEmberGameObjectList[i]));
            //campEmberParticleSystemList[i].Stop();
        }
        
        // Water Lake
        waterLakeGameObject = findGameObject("water_lake");
        waterLakeOriginalLoctionVector = new Vector3(-2.66f, -0.19f, 0.6f);
        waterLakeFinalLoctionVector = new Vector3(-2.66f, -1.35f, 0.6f);
        waterLakeGameObject.transform.localPosition = waterLakeFinalLoctionVector;

        // Dragon
        dragonCommodityController = findGameObject("CommodityController");
        dragonGameObject = findGameObject("Dragon");
        dragonStationaryAnimation = findGameObject("SJ001").GetComponent<Animation>();
        dragonStationaryAnimation.Play();

        flameThrowerGameObject = findGameObject("Flamethrower");
        flameThrowerAudioSource = flameThrowerGameObject.GetComponent<AudioSource>();
        flameThrowerAudioSource.Stop();

        flameThrowerFlameGameObject = findGameObject("FlamethrowerFire");
        flameThrowerFlameParticleSystem = findParticleSystem(flameThrowerFlameGameObject);
        flameThrowerFlameParticleSystem.Stop();

        flameThrowerSmokeGameObject = findGameObject("FlamethrowerSmoke");
        flameThrowerSmokeParticleSystem = findParticleSystem(flameThrowerSmokeGameObject);
        flameThrowerSmokeParticleSystem.Stop();

        dragonFiringPositionList = new List<Vector3>();
        dragonFiringPositionList.Add(new Vector3(-21.08f, 3f, -31.53f));
        dragonFiringPositionList.Add(new Vector3(-11.48f, 3f, -26.6f));
        dragonFiringPositionList.Add(new Vector3(-23.87f, 2.26f, -19.44f));

        dragonRotationValuesList = new List<Quaternion>();
        dragonRotationValuesList.Add(Quaternion.Euler(new Vector3(31f, 75.62f, 0f)));
        dragonRotationValuesList.Add(Quaternion.Euler(new Vector3(31f, -78.8f, 0f)));
        dragonRotationValuesList.Add(Quaternion.Euler(new Vector3(13.22f, 149.86f, 5.72f)));
    }

    // Start is called before the first frame update
    void Start()
    {
        maxCycle.GetComponent<Text>().text = "Cycles left: " + max_cycle;
        scoreText.GetComponent<Text>().text = "Successful Cycles: " + 0;
        // print("Max");

        initializeObejects();
        if (use_spirometer)
        {
            oscGameObject.SetActive(true);
            oscScript.SetAddressHandler("/m5Analog", OnReceiveBreathData);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (gameFinished)
        {
            // print("inside startup pahse loop");
            return;
        }
        // if (!use_spirometer)
        {
            checkForKeyPress();
        }
        run();
    }

    //Landing Page
    public void ToGame(int maximum_cycle_arg, int inhalation_arg, int exhalation_arg)
    {
        inhalation_time = inhalation_arg;
        exhalation_time = exhalation_arg;
        max_cycle = maximum_cycle_arg;

        // print(inhalation_time);
        // print(exhalation_time);
        // print(max_cycle);

        maxCycle.GetComponent<Text>().text = "Cycles left: " + max_cycle;

        SubmitGameObject = findGameObject("LandingPage");
        if (SubmitGameObject)
        {
            SubmitGameObject.SetActive(false);
        }
        
        uiHelper = findGameObject("UIHelpers");
        if (uiHelper)
        {
            uiHelper.SetActive(false);
        }

        ovrCamerRig = findGameObject("OVRCameraRig");
        if (ovrCamerRig)
        {
            ovrCamerRig.SetActive(false);
        }
        if (stats)
        {
            stats.SetActive(true);
        }
        currentPhase = PHASES.DRAGON;
    }

    // When spirometer is connected
    void OnReceiveBreathData(OscMessage message)
    {
        if (currentPhase == PHASES.STARTUP || currentPhase == PHASES.DRAGON || currentPhase == PHASES.WATER_LAKE_FILL || currentPhase == PHASES.DRAON_STARTED || currentPhase == PHASES.WATER_LAKE_FILL_STARTED || currentPhase == PHASES.STARTING_FIRE_AGAIN)
        {
            return;
        }
        // print("enter ReceivedData");
        float breath_value = message.GetFloat(0);
        // print("Received breath value: " + breath_value);

        // Perform action based on breath value range
        if (currentPhase == PHASES.INHALE  && breath_value > inhaling_expected_force)//inhale
        {
            // print("Received breath value: " + breath_value);
            currentBreathingState = BREATHING_STATE.INHALE;
        }
        else if (currentPhase == PHASES.EXHALE && breath_value < exhaling_expected_force)
        {
            // print("Received breath value: " + breath_value);
            currentBreathingState = BREATHING_STATE.EXHALE;
        }
        else
        {
            inhale_exhale_started = false;
            currentBreathingState = BREATHING_STATE.PAUSE;
        }
    }

    // To handle cases when spirometer isn't connected
    void checkForKeyPress()
    {
        if (currentPhase == PHASES.STARTUP || currentPhase == PHASES.DRAGON || currentPhase == PHASES.WATER_LAKE_FILL || currentPhase == PHASES.DRAON_STARTED || currentPhase == PHASES.WATER_LAKE_FILL_STARTED || currentPhase == PHASES.STARTING_FIRE_AGAIN)
        {
            return;
        }

        OVRInput.Update();

        if (currentPhase == PHASES.INHALE && (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) || Input.GetKeyDown("a") || Input.GetKeyDown(KeyCode.Space)))
        {
            currentBreathingState = BREATHING_STATE.INHALE;
        }
        else if (currentPhase == PHASES.EXHALE && (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) || Input.GetKeyDown("z") || Input.GetKeyDown(KeyCode.Space)))
        {
            currentBreathingState = BREATHING_STATE.EXHALE;
           

        }
        else if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger) || Input.GetKeyUp("a") || Input.GetKeyUp(KeyCode.Space) || Input.GetKeyUp("z"))
        {
            inhale_exhale_started = false;
            currentBreathingState = BREATHING_STATE.PAUSE;
        }
    }

    void run()
    {
        if (currentPhase == PHASES.STARTUP || currentPhase == PHASES.DRAON_STARTED || currentPhase == PHASES.WATER_LAKE_FILL_STARTED || currentPhase == PHASES.STARTING_FIRE_AGAIN)
        {
            return;
        }
        if (inhale_exhale_started && (currentBreathingState == BREATHING_STATE.EXHALE || currentBreathingState == BREATHING_STATE.INHALE))
        {
            return;
        }
        if (currentPhase == PHASES.DRAGON)
        {
            // Move Dragon to next position and start fire
            currentPhase = PHASES.DRAON_STARTED;
            StartCoroutine(move_dragon_to_next_position());
        }
        else if (currentPhase == PHASES.WATER_LAKE_FILL)
        {
            // Fill water in Lake
            currentPhase = PHASES.WATER_LAKE_FILL_STARTED;
            lake_filled = true;
            StartCoroutine(fill_water_in_lake());
        }
        else if (currentPhase == PHASES.INHALE && currentBreathingState == BREATHING_STATE.INHALE)
        {
            inhalation_total++;
            inhale_exhale_started = true;
            StartCoroutine(inhale_from_water_lake());
        }
        else if (!lake_filled && currentPhase == PHASES.INHALE && currentBreathingState == BREATHING_STATE.PAUSE)
        {
            inhaleAudio.Stop();
            // Inhale not completely done. So fill the water again and start the cycle
            if (!inhale_completely_done)
            {
                failParticleSystem.Play();
                if (failAudio != null) {
                    failAudio.Play();
                    print("Playing fail audio!");
                }
           
                currentPhase = PHASES.WATER_LAKE_FILL;
                inhaleParticleSystem.Stop();
                inhalation_failed++;
                cycle_failed++;
                complete_cycle();
                if (gameFinished)
                {
                    return;
                }
                StopAllCoroutines();
            }
        }
        else if (currentPhase == PHASES.EXHALE && currentBreathingState == BREATHING_STATE.EXHALE)
        {
            inhalation_success++;
            exhalation_total++;
            inhale_exhale_started = true;
            StartCoroutine(put_out_camp_fire());
        }
        else if (fire_out && currentPhase == PHASES.EXHALE && currentBreathingState == BREATHING_STATE.PAUSE)
        {
            // Exhale not completely done. So fill the water again and start the cycle
            exhaleAudio.Stop();
            if (!exhale_completely_done)
            {   
                failParticleSystem.Play();
                if (failAudio != null) {
                    failAudio.Play();
                    print("Playing fail audio!");
                }
           // cheerAudio.Stop();
                currentPhase = PHASES.STARTING_FIRE_AGAIN;
                waterGunParticleSystem.Stop();
                StopAllCoroutines();
                exhalation_failed++;
                cycle_failed++;
                complete_cycle();
                if (gameFinished)
                {
                    return;
                }
                StartCoroutine(start_fire_on_camp());
            }
        }
        else if (currentPhase == PHASES.CYCLE_DONE)
        {
            currentPhase = PHASES.DRAGON;
            inhale_completely_done = false;
            exhale_completely_done = false;
            // failParticleSystem.Stop();
            inhale_exhale_started = false;
            int next_target = Random.Range(0, totalTargets);
            while(next_target == currentTarget)
            {
                next_target = Random.Range(0, totalTargets);
            }
            currentTarget = next_target;

            exhalation_success++;
            successful_cycle++;

            star1ParticleSystem.Play();
            if (successAudio != null) {
                    successAudio.Play();
                    print("Playing success audio!");
            }
            scoreText.GetComponent<Text>().text = "Successful Cycles: " + successful_cycle;

            complete_cycle();
            if (gameFinished)
            {
                return;
            }
            waterGunGameObject.transform.rotation = waterGunRotationValues[currentTarget];
        }
    }

    void complete_cycle()
    {
        completed_cycles += 1;
        int cycle_left = max_cycle - completed_cycles;
        maxCycle.GetComponent<Text>().text = "Cycles left: " + cycle_left;
        // DD write code for end game
        print("cycle done " + completed_cycles);
        print("cycle failed " + cycle_failed);
        print("cycle sccessful " + successful_cycle);
        print("cycle total " + max_cycle);
        print("cycle left " + cycle_left);
        bool result = completed_cycles >= max_cycle;
        print("result: " + (result));
        if (result)
        {
            StopAllCoroutines();
            CancelInvoke();
            print("inside if");
            currentBreathingState = BREATHING_STATE.PAUSE;
            currentPhase = PHASES.STARTUP;
            oscScript.Close();
            gameFinished = true;
            StartCoroutine(gameover());
            
        }
    }

    IEnumerator gameover()
    {
            // print(GameOverGameObject);
            // print(currentPhase);
            yield return new WaitForSeconds(1f);

            // uiHelper.SetActive(true);
            // ovrCamerRig.SetActive(true);
            if (stats)
            {
                stats.SetActive(false);
            }
            if (GameOverGameObject)
            {
                GameOverGameObject.SetActive(true);
            }

            celebrationParticleSystem.Play();
            if (cheerAudio != null) {
                cheerAudio.Play();
                print("Playing cheer audio!");
            }
           // cheerAudio.Stop();
            print("loop done");
            print("exhalation failed " + exhalation_failed);
            print("inhalation failed " + inhalation_failed);
            print("exhalation success " + exhalation_success);
            print("inhalation success " + inhalation_success);
            print("exhalation total " + exhalation_total);
            print("inhalation total " + inhalation_total);
            print("cycle failed " + cycle_failed);
            print("cycle sccessful " + successful_cycle);
            print("cycle total " + max_cycle);
            successfultext.GetComponent<Text>().text = "Successful Cycles: " + successful_cycle;
            inhalationtext.GetComponent<Text>().text = "Inhale Stats: " + inhalation_success + " / " + max_cycle;
            exhalationtext.GetComponent<Text>().text = "Exhale Stats: " + exhalation_success + " / " + exhalation_total;
            totaltext.GetComponent<Text>().text = "Total Cycles: " + max_cycle;
    }
    // Dragon Coroutines
    IEnumerator move_dragon_to_next_position()
    {
        // print("move_dragon_to_next_position");
        yield return new WaitForSecondsRealtime(1);
        dragonStationaryAnimation.Stop();
        while (dragonCommodityController.transform.localPosition != dragonFiringPositionList[currentTarget])
        {
            dragonCommodityController.transform.localPosition = Vector3.MoveTowards(dragonCommodityController.transform.position, dragonFiringPositionList[currentTarget], dragonMovementSpeed);
            yield return null;
        }
        
        while (dragonCommodityController.transform.rotation != dragonRotationValuesList[currentTarget])
        {
            dragonCommodityController.transform.rotation = Quaternion.RotateTowards(dragonCommodityController.transform.rotation, dragonRotationValuesList[currentTarget], dragonRotationSpeed);
            yield return null;
        }
        StartCoroutine(start_fire_on_camp_with_dragon());
    }

    // Campfire Coroutines
    IEnumerator start_fire_on_camp_with_dragon()
    {
        flameThrowerFlameParticleSystem.Play();
        flameThrowerSmokeParticleSystem.Play();
        flameThrowerAudioSource.Play();
        yield return null;
        while (campFlameGameObjectList[currentTarget].transform.localScale != fireFlameOriginalScale)
        {
            campFlameGameObjectList[currentTarget].transform.localScale = Vector3.MoveTowards(campFlameGameObjectList[currentTarget].transform.localScale, fireFlameOriginalScale, fireIncreaseRate);
            yield return null;
        }
        campEmberGameObjectList[currentTarget].transform.localScale = fireEmberOriginalScale;
        flameThrowerFlameParticleSystem.Stop();
        flameThrowerSmokeParticleSystem.Stop();
        flameThrowerAudioSource.Stop();
        dragonStationaryAnimation.Play();
        currentPhase = PHASES.WATER_LAKE_FILL;
        fire_out = false;
    }

    IEnumerator start_fire_on_camp()
    {
        while (campFlameGameObjectList[currentTarget].transform.localScale != fireFlameOriginalScale)
        {
            campFlameGameObjectList[currentTarget].transform.localScale = Vector3.MoveTowards(campFlameGameObjectList[currentTarget].transform.localScale, fireFlameOriginalScale, fireIncreaseRate);
            yield return null;
        }
        campEmberGameObjectList[currentTarget].transform.localScale = fireEmberOriginalScale;
        currentPhase = PHASES.WATER_LAKE_FILL;
        inhale_completely_done = false;
        fire_out = false;
    }

    IEnumerator put_out_camp_fire()
    {
        waterGunParticleSystem.Play();
        fire_out = true;
        yield return new WaitForSecondsRealtime(0.4f);
        start_test_time();
        if (exhaleAudio != null) {
            exhaleAudio.Play();
        }
        while (campFlameGameObjectList[currentTarget].transform.localScale != zeroScale)
        {
            campFlameGameObjectList[currentTarget].transform.localScale = Vector3.MoveTowards(campFlameGameObjectList[currentTarget].transform.localScale, zeroScale, exhalation_speed[exhalation_time - 1]);
            yield return new WaitForSecondsRealtime(exhalation_wait_time[exhalation_time - 1]);
        }
        exhaleAudio.Stop();
        stop_test_time("exhalation done");
        waterGunParticleSystem.Stop();
        exhale_completely_done = true;
        //scoreText.GetComponent<Text>().text = "Completed Cycles: " + score;
        campEmberGameObjectList[currentTarget].transform.localScale = zeroScale;
        yield return new WaitForSecondsRealtime(0.6f);
        currentPhase = PHASES.CYCLE_DONE;
    }

    // Water Lake Coroutines
    IEnumerator fill_water_in_lake()
    {
        while (waterLakeGameObject.transform.localPosition != waterLakeOriginalLoctionVector)
        {
            waterLakeGameObject.transform.localPosition = Vector3.MoveTowards(waterLakeGameObject.transform.localPosition, waterLakeOriginalLoctionVector, water_lake_fill_speed);
            yield return null;
        }
        currentPhase = PHASES.INHALE;
    }

    IEnumerator inhale_from_water_lake()
    {
        lake_filled = false;
        inhaleParticleSystem.Play();
        start_test_time();
        if (inhaleAudio != null) {
            inhaleAudio.Play();
            print("Playing inhale audio!");
        }
        while (waterLakeGameObject.transform.localPosition != waterLakeFinalLoctionVector)
        {
            waterLakeGameObject.transform.localPosition = Vector3.MoveTowards(waterLakeGameObject.transform.localPosition, waterLakeFinalLoctionVector, inhalation_speed[inhalation_time - 1]);
            yield return new WaitForSecondsRealtime(inhalation_wait_time[inhalation_time - 1]);
        }
        inhaleAudio.Stop();
        print("stopped audio");
        stop_test_time("inhalation");
        inhaleParticleSystem.Stop();
        inhale_completely_done = true;
        currentPhase = PHASES.EXHALE;
    }

    void start_test_time()
    {
        watch = System.Diagnostics.Stopwatch.StartNew();
    }

    void stop_test_time(string name)
    {
        watch.Stop();
        var elapsedMs = watch.ElapsedMilliseconds;
        print(name + " Time Elapsed " + elapsedMs);
    }
}
