﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;


public class play_game : MonoBehaviour
{

    public finalized_script finalized_script_Object;
    public Text max_cycle;
    public Text inhalation_cycle;
    public Text exhalation_cycle;

    GameObject findGameObject(string name)
    {
        return GameObject.Find(name);
    }

    public void start_game()
    {
        finalized_script_Object.ToGame(int.Parse(max_cycle.text), int.Parse(inhalation_cycle.text), int.Parse(exhalation_cycle.text));
    }

    public void restart_game()
    {
        SceneManager.LoadScene("DemoScene2");
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
