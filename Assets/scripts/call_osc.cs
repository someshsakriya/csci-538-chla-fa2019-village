﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class call_osc : MonoBehaviour {

   	public OSC osc;
    Animator anim;
    int count = 0;
    Animator anim1;
    Animator anim2;
    Animator cubebounce;
    public int flag = 0;

    bool inhaleDone = false;
    bool inhaleDone1 = false;


    // Use this for initialization
    void Start () {
        print("just started");
        //var lid = GameObject.Find("lid_pivot").gameObject;
        var cube = GameObject.Find("Cube").gameObject;
        cubebounce = cube.GetComponent<Animator>();

        var lid = GameObject.Find("Cube").transform.Find("lid_pivot").gameObject;
        
        anim = lid.GetComponent<Animator>();
        var lid1 = GameObject.Find("Cube1").transform.Find("lid_pivot1").gameObject;

        anim1 = lid1.GetComponent<Animator>();
        var lid2 = GameObject.Find("Cube2").transform.Find("lid_pivot2").gameObject;

        anim2 = lid2.GetComponent<Animator>();


    }
	
	// Update is called once per frame
	void Update () {
        if (flag == 0)
        {
            
            osc.SetAddressHandlerCustom("/m5Analog", OnReceiveBreathData);
        }
        else if (flag == 1)
        {
            print("*************************************************************");
            osc.SetAddressHandlerCustom("/m5Analog", OnReceiveBreathData2);

        }
        else
        {
            print("*************************************************************");
            osc.SetAddressHandlerCustom("/m5Analog", OnReceiveBreathData3);
        }

    }

    void OnReceiveBreathData(OscMessage message)
    {

        float breath_value = message.GetFloat(0);
        print("Received breath value: " + breath_value);

        // Perform action based on breath value range
        if (flag == 0)
        {
            if (breath_value < 1800)//exhale
            {
                print("exhaling box1!!!!!!");
                moveLeft();
                flag = 1;
            }
            else   //inhale
            {
                if (breath_value > 2500)
                {
                    print("inhaling box1!!");
                    cubebounce.SetInteger("bounce", 1);
                    moveRight();

                }
            }
        }
    }


    void moveLeft() {
        //int moveSpeed = 1;
        // transform.position = new Vector3(transform.position.x + moveSpeed, transform.position.y);
        //transform.Translate(moveSpeed, 0, 0);
        //print("count for exhale:" + count);
        //print(".......");
        anim.SetInteger("anim_state", 1);
        count += 1;
    }
    void moveRight()
    {
        //print("count for inhale:" + count);
        //var gift = GameObject.Find("lid_pivot").gameObject;
        var cube = GameObject.Find("Cube").gameObject;
        //print("**********");
        //int moveSpeed = 1;
        // transform.position = new Vector3(transform.position.x + moveSpeed, transform.position.y);
        //transform.Translate(moveSpeed, 0, 0);
        //anim.SetInteger("anim_state", 1);
       // gift.transform.localScale = new Vector3(1.75f, 1.0f, 1.25f);
        //gift.transform.position = new Vector3(10.491f, 1.25f, -0.75903f);
        cube.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
    }





    void OnReceiveBreathData2(OscMessage message)
    {
        float breath_value = message.GetFloat(0);
        print("2.Received breath value: " + breath_value);

        // Perform action based on breath value range
        
            
            if (breath_value > 2500)
            {
                print("inhaling box2!!");
                moveRight2();
                inhaleDone = true;
                

            }
            else   //inhale
            {
                if (breath_value < 1800 && inhaleDone) //exhale
                {
                    print("exhaling box2!!!!!!");
                    moveLeft2();
                    flag = 2;
            }
            }
    }




    void moveLeft2()
    {
        //int moveSpeed = 1;
        // transform.position = new Vector3(transform.position.x + moveSpeed, transform.position.y);
        //transform.Translate(moveSpeed, 0, 0);
        //print("count for exhale:" + count);
        //print(".......");
        anim1.SetInteger("anim_state", 1);
        count += 1;
    }
    void moveRight2()
    {
        //print("count for inhale:" + count);
        //var gift = GameObject.Find("lid_pivot1").gameObject;
        var cube = GameObject.Find("Cube1").gameObject;
        //print("**********");
        //int moveSpeed = 1;
        // transform.position = new Vector3(transform.position.x + moveSpeed, transform.position.y);
        //transform.Translate(moveSpeed, 0, 0);
        //anim.SetInteger("anim_state", 1);
        //gift.transform.localScale = new Vector3(1.75f, 1.0f, 1.25f);
        //gift.transform.position = new Vector3(10.07508f, 1.09f, -2.81f);
        cube.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
    }

    void OnReceiveBreathData3(OscMessage message)
    {
        
        float breath_value = message.GetFloat(0);
        print("3.Received breath value: " + breath_value);

        // Perform action based on breath value range


        if (breath_value > 2500)
        {
            print("inhaling box3!!");
            moveRight3();
            inhaleDone1 = true;


        }
        else   //inhale
        {
            if (breath_value < 1800 && inhaleDone1) //exhale
            {
                print("exhaling box3!!!!!!");
                moveLeft3();
            }
        }
    }




    void moveLeft3()
    {
        //int moveSpeed = 1;
        // transform.position = new Vector3(transform.position.x + moveSpeed, transform.position.y);
        //transform.Translate(moveSpeed, 0, 0);
        //print("count for exhale:" + count);
        //print(".......");
        anim2.SetInteger("anim_state", 1);
        count += 1;
    }
    void moveRight3()
    {
        // print("count for inhale:" + count);
        //var gift = GameObject.Find("lid_pivot2").gameObject;
        var cube = GameObject.Find("Cube2").gameObject;
        //print("**********");
        //int moveSpeed = 1;
        // transform.position = new Vector3(transform.position.x + moveSpeed, transform.position.y);
        //transform.Translate(moveSpeed, 0, 0);
        //anim.SetInteger("anim_state", 1);
        //gift.transform.localScale = new Vector3(1.75f, 1.0f, 1.25f);
        //gift.transform.position = new Vector3(10.28f, 1.423f, 1.27f);
        cube.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
    }
}