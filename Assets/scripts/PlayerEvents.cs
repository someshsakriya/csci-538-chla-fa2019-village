﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerEvents : MonoBehaviour
{


    #region Anchors
    public GameObject m_LeftAnchor;
    public GameObject m_RightAnchor;
    public GameObject m_HeadAnchor;
    #endregion

    #region Input

    private Dictionary<OVRInput.Controller, GameObject> m_ControllerSets = null;
    private OVRInput.Controller m_InputSource = OVRInput.Controller.None;
    private OVRInput.Controller m_Controller = OVRInput.Controller.None;
    private bool m_InputActive = true;
    #endregion
    // Start is called before the first frame update
    private void Awake()
    {
        OVRManager.HMDMounted += PlayerFound;
        OVRManager.HMDUnmounted += PlayerLost;
        m_ControllerSets = CreateControllerSets();
    }

    private void OnDestroy()
    {
        OVRManager.HMDMounted -= PlayerFound;
        OVRManager.HMDUnmounted -= PlayerLost;


    }
    private void Update()
    {

        OVRInput.Update();

        CheckForController();
        CheckInputSource();
        Input();


    }
    private void CheckForController()
    {



    }

    private void CheckInputSource()
    { 



    }

    private void Input()
    {


    }

    private void PlayerFound()
    {

        m_InputActive = true;
    }


    private void PlayerLost()
    {


        m_InputActive =false;
    }

    private Dictionary<OVRInput.Controller,GameObject> CreateControllerSets()
    {
        Dictionary<OVRInput.Controller, GameObject> newSets = new Dictionary<OVRInput.Controller, GameObject>()
        {
            {OVRInput.Controller.LTrackedRemote,m_LeftAnchor },
            {OVRInput.Controller.RTrackedRemote,m_RightAnchor },
            {OVRInput.Controller.Touchpad,m_HeadAnchor }
        };

        return newSets;
    }
}
