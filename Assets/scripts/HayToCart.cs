﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HayToCart : MonoBehaviour
{
    GameObject go, FinalInhalationHay, leftHay, rightHay, midHay, cartLeftHay, cartRightHay, cartMidHay, FinalPauseHay;
    public Vector3 DifferenceIn, DifferenceEx, final, start, AfterIn, AfterPa, DifferencePause;
    int count = 0;
    void Start()
    {
        leftHay = GameObject.Find("Left");
        rightHay = GameObject.Find("Right");
        midHay = GameObject.Find("Mid");
        cartLeftHay = GameObject.Find("CartLeft");
        cartMidHay = GameObject.Find("CartMid");
        cartRightHay = GameObject.Find("CartRight");
        FinalInhalationHay = GameObject.Find("InhaleFinal");
        FinalPauseHay = GameObject.Find("PauseFinal");
        
        AfterIn = FinalInhalationHay.transform.localPosition;
        AfterPa = FinalPauseHay.transform.localPosition;

    }

    // Update is called once per frame
    void Update()
    {
        if (count < 9)
        {
            // INHALE
            if (Input.GetKeyDown("j"))
            {
                if (count == 0 || count == 3 || count == 6)
                {
                    if (count == 0)
                    {
                        go = leftHay;
                        start = go.transform.localPosition;
                        DifferenceIn = AfterIn - start;
                    }
                    if (count == 3)
                    {
                        go = rightHay;
                        start = go.transform.localPosition;
                        DifferenceIn = AfterIn - start;
                    }
                    if (count == 6)
                    {
                        go = midHay;
                        start = go.transform.localPosition;
                        DifferenceIn = AfterIn - start;
                    }
                    //go.transform.localPosition = Vector3.MoveTowards(go.transform.localPosition, Point1, 0.09f);

                    while (go.transform.localPosition != AfterIn)
                    {
                        
                        go.transform.localPosition += DifferenceIn / 30.0f;
                    }
                    count++;
                }
            }



            //PAUSE
            if (Input.GetKeyDown("k"))
            {
                if (count == 1 || count == 4 || count == 7)
                {
                    if (count == 1)
                    {
                        go = leftHay;
                        start = go.transform.localPosition;
                        DifferencePause = AfterPa - start;
                    }
                    if (count == 4)
                    {
                        go = rightHay;
                        start = go.transform.localPosition;
                        DifferencePause = AfterPa - start;
                    }
                    if (count == 7)
                    {
                        go = midHay;
                        start = go.transform.localPosition;
                        DifferencePause = AfterPa - start;
                    }
                    while (go.transform.localPosition != AfterPa)
                    {
                        go.transform.localPosition += DifferencePause / 30.0f;
                    }
                    count++;
                }
            }




            // EXHALE
            if (Input.GetKeyDown("l"))
            {
                if (count == 2 || count == 5 || count == 8)
                {
                    if (count == 2)
                    {
                        go = leftHay;
                        start = go.transform.localPosition;
                        final = cartLeftHay.transform.localPosition;
                        DifferenceEx = final - start;
                        while (go.transform.localPosition != final)
                        {
                            go.transform.localPosition += DifferenceEx / 30.0f;
                        }
                        go.SetActive(false);
                        cartLeftHay.SetActive(true);

                    }
                    if (count == 5)
                    {
                        go = rightHay;
                        start = go.transform.localPosition;
                        final = cartRightHay.transform.localPosition;
                        DifferenceEx = final - start;
                        while (go.transform.localPosition != final)
                        {
                            go.transform.localPosition += DifferenceEx / 30.0f;
                        }
                        go.SetActive(false);
                        cartRightHay.SetActive(true);
                    }
                    if (count == 8)
                    {
                        go = midHay;
                        start = go.transform.localPosition;
                        final = cartMidHay.transform.localPosition;
                        DifferenceEx = final - start;
                        while (go.transform.localPosition != final)
                        {
                            go.transform.localPosition += DifferenceEx / 30.0f;
                        }
                        go.SetActive(false);
                        cartMidHay.SetActive(true);
                    }
                    count++;
                }
            }
        }
    }
}
