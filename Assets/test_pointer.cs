﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test_pointer : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject SubmitGameObject;
    enum PHASES {STARTUP, DRAGON, DRAON_STARTED, WATER_LAKE_FILL, WATER_LAKE_FILL_STARTED, INHALE, EXHALE, STARTING_FIRE_AGAIN, CYCLE_DONE};
    PHASES currentPhase = PHASES.DRAGON;
    GameObject findGameObject(string name)
    {
        return GameObject.Find(name);
    }
    public void Submit_Btn()
    {
        SubmitGameObject = findGameObject("Landing Page");
        print("Submit");
        SubmitGameObject.SetActive(false);
        currentPhase = PHASES.DRAGON;
    }
}
